<?php
session_start();

    $palabras = [
        "amarillo","sidney","spaghetti","electro","facebook","jeans",
        "envergadura","fresa","kinder","francisco"
    ];

    
    
    $gameover = [
        "imagen/ahorcado_0.png",
        "imagen/ahorcado_1.png",
        "imagen/ahorcado_2.png",
        "imagen/ahorcado_3.png",
        "imagen/ahorcado_4.png",
        "imagen/ahorcado_5.png",
        "imagen/ahorcado_6.png",
    ];
    
    
    require_once('elegir_palabra.php'); 
    require_once('dibujar_bloques.php');   
    require_once('escribir_formulario.php');
    require_once('numero_fallos.php');
    require_once('mostrar_intentos.php');
    require_once('acertar_palabra.php');
    require_once('fin_ahorcado.php');
    
    
?>   
<html>
    <head>
        <meta charset="UTF-8">
        <title>&#128302; ¡¡Adivina la palabra!! &#128526;</title>
    </head>
    <body align="center">
        <h1>&#129300; Adivina la palabra &#127808;</h1>
       
        
        <?php
        
            if(!isset($_SESSION["palabra"]))
                {
                    $_SESSION["palabra"] = elegir_palabra($palabras);
                }
            
            
            $posiciones = [];
            
            if($_REQUEST){     
                
                if(!isset($_SESSION["intentos"]))
                   {
                        $_SESSION["intentos"] = [ $_REQUEST['letra'] ];
                }else{
                        $_SESSION["intentos"][] = $_REQUEST['letra'];
                }
                
                        
                echo dibujar_bloques($_SESSION["palabra"],$_SESSION["intentos"]);
               
            }else{
                
                echo dibujar_bloques($_SESSION["palabra"],[]);
                
            }
                
           
            echo escribir_formulario();
            
            
            if(isset($_SESSION["intentos"])){
                
                echo mostrar_intentos($_SESSION["intentos"]);
                
                echo '<img src="'.$gameover[numero_fallos($_SESSION["palabra"], $_SESSION["intentos"])].'">';
                
                if(acertar_palabra($_SESSION["palabra"],$_SESSION["intentos"])){
                    
                    echo fin_ahorcado();
                    session_destroy();
                }
                
            }
                                   
             
        ?>
    </body>
</html>