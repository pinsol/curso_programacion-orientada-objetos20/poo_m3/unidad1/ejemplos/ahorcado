<?php
function acertar_palabra($palabra,$intentos){
    
    $is_acertado = true;
    
    for($i=0;$i<strlen($palabra);$i++){
    
        if(!in_array($palabra[$i],$intentos)){
            
            $is_acertado = false;
            break;
        } 
        
           
    }
    
    
    return $is_acertado;
      
    
}
